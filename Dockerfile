FROM ubuntu:18.04

RUN apt-get update --fix-missing
RUN apt-get upgrade -y
RUN apt-get install curl zsh wget nano -y
RUN apt-get install gdebi-core -y
RUN apt-get install libglu1-mesa -y
RUN apt-get install libglib2.0-0 -y
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -
RUN apt-get install nodejs -y
RUN apt-get install imagemagick -y
RUN apt-get install graphicsmagick -y
RUN npm install --global gulp-cli gulper
RUN apt-get install ruby-full -y
RUN gem install premailer

WORKDIR /html-email-generator

CMD ["/bin/zsh"]