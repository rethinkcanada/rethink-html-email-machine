const { readdirSync, readFileSync } = require("fs");
const { resolve } = require("path");
const getRootDir = require("root-directory");

const { unslugify } = require("../../src/components/tools/folderTools");

const {
  GET_CLIENT_LIST,
  RETURN_CLIENT_LIST,
  GET_DOCKET_LIST,
  RETURN_DOCKET_LIST,
  GET_EMAIL_LIST,
  RETURN_EMAIL_LIST,
} = require("../../src/components/store/actions/actionTypes");

module.exports = function startdevSocket(socketClient, sharedVariables) {
  const getDirectories = (source) =>
    readdirSync(source, { withFileTypes: true })
      .filter((dirent) => dirent.isDirectory())
      .map((dirent) => dirent.name)
      .filter((dir) => dir[0] !== "_")
      .filter((dir) => dir[0] !== ".")
      .filter((dir) => dir !== "rawfiles");

  const getClientList = () => {
    return new Promise(async (res, reject) => {
      const rootDir = await getRootDir();
      const clientListPath = resolve(rootDir, "../emails/");
      try {
        let directories = getDirectories(clientListPath);

        res(directories);
      } catch (e) {
        console.log(e);
        reject(e);
      }
    });
  };

  const getDocketList = (client) => {
    return new Promise(async (res, reject) => {
      const rootDir = await getRootDir();
      client = unslugify(client);
      const docketListPath = resolve(rootDir, `../emails/${client}/`);
      try {
        let directories = getDirectories(docketListPath);
        res(directories);
      } catch (e) {
        reject(e);
      }
    });
  };

  const getEmailList = (client, docket) => {
    return new Promise(async (res, reject) => {
      const rootDir = await getRootDir();
      client = unslugify(client);
      docket = unslugify(docket);
      const docketListPath = resolve(rootDir, `../emails/${client}/${docket}/`);
      try {
        let directories = getDirectories(docketListPath);
        res(directories);
      } catch (e) {
        reject(e);
      }
    });
  };

  const getFTPLink = (client, docket) => {
    return new Promise(async (res, reject) => {
      client = unslugify(client);
      docket = unslugify(docket);
      try {
        const clientJSON = JSON.parse(
          readFileSync(`../emails/${client}/client.config.json`)
        );
        const docketJSON = JSON.parse(
          readFileSync(`../emails/${client}/${docket}/docket.config.json`)
        );
        const ftpLink = `http://${clientJSON.ftpDomain}/docs/${docketJSON.docket}`;
        res(ftpLink);
      } catch (e) {
        res("");
      }
    });
  };

  socketClient.on("action", async (action) => {
    console.log(action);
    switch (action.type) {
      case GET_CLIENT_LIST:
        const clients = await getClientList();
        socketClient.emit("action", {
          type: RETURN_CLIENT_LIST,
          payload: {
            clients,
          },
        });
        break;
      case GET_DOCKET_LIST:
        const dockets = await getDocketList(action.client);
        socketClient.emit("action", {
          type: RETURN_DOCKET_LIST,
          payload: {
            dockets,
          },
        });
        break;
      case GET_EMAIL_LIST:
        const banners = await getEmailList(action.client, action.docket);
        const ftplink = await getFTPLink(action.client, action.docket);
        socketClient.emit("action", {
          type: RETURN_EMAIL_LIST,
          payload: {
            banners,
            ftplink,
          },
        });
        break;
      default:
        return;
    }
  });
};
