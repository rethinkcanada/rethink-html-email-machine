const { spawn } = require("child_process");
const stripAnsi = require("strip-ansi");

const {
  CREATE_EMAIL,
  CREATE_EDIT_COMPLETE,
  CREATE_EDIT_ERROR,
  CREATE_DOCKET_FOLDER,
  CREATE_CLIENT_FOLDER,
} = require("../../src/components/store/actions/actionTypes");

module.exports = function createEditSocket(socketClient, sharedVariables) {
  const createNewEmail = ({
    folder,
    emailname,
    emailWidth,
    imageSubfolder,
    emailSubject,
    emailPreview,
    emailFromAddress,
    mainLink,
  }) => {
    let errorFlag = false;
    let errorMessage = [];
    // TODO: need to pass in the folder the email will be put into, forgot to do that
    const newEmailProcess = spawn("gulp", [
      "createNewEmail",
      "--folder",
      folder,
      "--emailname",
      emailname,
      "--emailWidth",
      emailWidth,
      "--imageSubfolder",
      imageSubfolder,
      "--emailSubject",
      emailSubject,
      "--emailPreview",
      emailPreview,
      "--emailFromAddress",
      emailFromAddress,
      "--mainLink",
      mainLink,
    ]);
    newEmailProcess.stdout.on("data", (data) => {
      console.log(data.toString());
    });
    newEmailProcess.stderr.on("data", (data) => {
      console.log(data.toString());
      errorFlag = true;
      errorMessage.push(stripAnsi(data.toString()));
    });
    newEmailProcess.on("exit", (code) => {
      if (!errorFlag) {
        socketClient.emit("action", { type: CREATE_EDIT_COMPLETE });
      } else {
        socketClient.emit("action", {
          type: CREATE_EDIT_ERROR,
          error: errorMessage.toString(),
        });
      }
    });
  };

  const createNewDocket = ({
    folder,
    docketNumber,
    projectName,
    imageServer,
  }) => {
    let errorFlag = false;
    let errorMessage = [];
    // TODO: need to pass in the folder the banner will be put into, forgot to do that
    const newDocketProcess = spawn("gulp", [
      "createNewDocket",
      "--folder",
      folder,
      "--docketnum",
      docketNumber,
      "--projectname",
      projectName,
      "--imageServer",
      imageServer,
    ]);
    newDocketProcess.stdout.on("data", (data) => {
      console.log(data.toString());
    });
    newDocketProcess.stderr.on("data", (data) => {
      console.log(data.toString());
      errorFlag = true;
      errorMessage.push(stripAnsi(data.toString()));
    });
    newDocketProcess.on("exit", (code) => {
      if (!errorFlag) {
        socketClient.emit("action", { type: CREATE_EDIT_COMPLETE });
      } else {
        socketClient.emit("action", {
          type: CREATE_EDIT_ERROR,
          error: errorMessage.toString(),
        });
      }
    });
  };

  const createNewClient = ({ clientName, ftpDomain }) => {
    let errorFlag = false;
    let errorMessage = [];

    const newClientProcess = spawn("gulp", [
      "createNewClient",
      "--clientname",
      clientName,
      "--ftpdomain",
      ftpDomain,
    ]);
    newClientProcess.stdout.on("data", (data) => {
      console.log(data.toString());
    });
    newClientProcess.stderr.on("data", (data) => {
      console.log(data.toString());
      errorFlag = true;
      errorMessage.push(stripAnsi(data.toString()));
    });
    newClientProcess.on("exit", (code) => {
      if (!errorFlag) {
        socketClient.emit("action", { type: CREATE_EDIT_COMPLETE });
      } else {
        socketClient.emit("action", {
          type: CREATE_EDIT_ERROR,
          error: errorMessage.toString(),
        });
      }
    });
  };

  socketClient.on("action", (action) => {
    switch (action.type) {
      case CREATE_EMAIL:
        createNewEmail(action.payload);
        break;
      case CREATE_DOCKET_FOLDER:
        createNewDocket(action.payload);
        break;
      case CREATE_CLIENT_FOLDER:
        createNewClient(action.payload);
        break;
      default:
        break;
    }
  });
};
