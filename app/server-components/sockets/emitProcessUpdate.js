const stripAnsi = require("strip-ansi");

const {
  TERMINAL_PROCESS_UPDATE
} = require("../../src/components/store/actions/actionTypes");

// module.exports = function emitProcessUpdate(socketClient, type, data) {
//   socketClient.emit(TERMINAL_PROCESS_UPDATE, {
//     method: type,
//     payload: [data.toString()]
//   });
// };

module.exports = function emitProcessUpdate(socketClient, type, data) {
  socketClient.emit("action", {
    type: TERMINAL_PROCESS_UPDATE,
    payload: {
      method: type,
      data: [stripAnsi(data.toString())]
    }
  });
};
