const stripAnsi = require("strip-ansi");
const { spawn } = require("child_process");

const emitProcessUpdate = require("./emitProcessUpdate");
const localhostRegex = /(Local:\shttp:\/\/localhost:\d\d\d\d)/g;
const getURLRegex = /(http:\/\/localhost:\d\d\d\d)/g;
const getPortRegex = /(\d\d\d\d)/g;

const {
  START_DEV,
  STOP_DEV,
  BROWSERSYNC_READY
} = require("../../src/components/store/actions/actionTypes");

module.exports = function startdevSocket(socketClient, sharedVariables) {
  let subProcess;

  const startupDevProcess = (bannerfolder, bannername) => {
    console.log("starting dev => " + bannerfolder);
    subProcess = spawn("gulper", [
      "dev",
      "--folder",
      bannerfolder,
      "--bannername",
      bannername
    ]);
    subProcess.stdout.setEncoding("utf8");
    subProcess.stdout.on("data", data => {
      console.log(data);
      const str = stripAnsi(data);
      const browserSyncLoc = str.toString().match(localhostRegex);
      if (browserSyncLoc) {
        const port = browserSyncLoc[0].match(getPortRegex)[0];
        sharedVariables.port = port;
        socketClient.emit("action", {
          type: BROWSERSYNC_READY,
          payload: {
            location: browserSyncLoc[0].match(getURLRegex)[0],
            port: port
          }
        });
      }
      emitProcessUpdate(socketClient, "result", str);
    });

    subProcess.stderr.on("data", data => {
      emitProcessUpdate(socketClient, "error", stripAnsi(data));
    });

    subProcess.on("exit", code => {
      console.log("browsersync process exited");
      emitProcessUpdate(socketClient, "warn", ["Gulp Process terminated!"]);
    });
  };

  const killProcess = () => {
    if (typeof subProcess !== "undefined") {
      subProcess.kill("SIGTERM");
    }

    spawn("gulp", ["enddev", "--folder", sharedVariables.bannerfolder]);
  };

  socketClient.on("action", action => {
    switch (action.type) {
      case START_DEV:
        sharedVariables.bannerfolder = action.payload.fullpath;
        startupDevProcess(
          sharedVariables.bannerfolder,
          action.payload.bannername
        );
        break;
      case STOP_DEV:
        killProcess();
        break;
      default:
        return;
    }
  });

  socketClient.on("disconnect", () => {
    console.log("disconnecting socket client");
    killProcess();
  });
};
