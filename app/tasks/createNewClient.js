module.exports = function () {
  const fs = require("fs");
  const replace = require("gulp-string-replace");
  const PluginError = require("plugin-error");
  const { src, dest } = require("gulp");

  const getParams = require("./getParams");

  const newClientData = getParams.getClientParams();
  const newFolderName = newClientData.clientName;

  if (fs.existsSync(`../banners/${newFolderName}`)) {
    throw new PluginError(
      "client",
      `the client folder ${newFolderName} already exists!`
    );
  }

  let srcTemplate = ["templates/folderList/client/**/*"];

  return src(srcTemplate, { allowEmpty: true })
    .pipe(
      replace("%CreateNewClientName%", () => {
        return newClientData.clientName;
      })
    )
    .pipe(
      replace("%CreateNewFTPDomain%", () => {
        return newClientData.ftpDomain;
      })
    )
    .pipe(dest(`../emails/${newFolderName}`));
};
