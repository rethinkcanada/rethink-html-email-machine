module.exports = function () {
  const fs = require("fs");
  const replace = require("gulp-string-replace");
  const PluginError = require("plugin-error");
  const { src, dest } = require("gulp");

  const getParams = require("./getParams");

  const folder = getParams.getFolder();
  const newEmailData = getParams.getNewEmailParams();
  const emailPathDest = getParams.getEmailPath(folder);

  if (fs.existsSync(`${emailPathDest}/${newEmailData.emailname}`)) {
    throw new PluginError(
      "email",
      `the email ${newEmailData.emailname} already exists!`
    );
  }

  let srcTemplate = ["templates/email/**/*"];

  return (
    src(srcTemplate, { allowEmpty: true })
      .pipe(
        replace("%FileName%", () => {
          return newEmailData.emailname;
        })
      )
      .pipe(
        replace("%MainLink%", () => {
          return newEmailData.mainLink;
        })
      )
      .pipe(
        replace("%EmailWidth%", () => {
          return newEmailData.emailWidth;
        })
      )
      .pipe(
        replace("%ImageServerSubfolder%", () => {
          return newEmailData.imageServerSubfolder;
        })
      )
      .pipe(
        replace("%SubjectText%", () => {
          return newEmailData.emailSubject;
        })
      )
      .pipe(
        replace("%PreviewText%", () => {
          return newEmailData.emailPreview;
        })
      )
      .pipe(
        replace("%FromEmail%", () => {
          return newEmailData.emailFromAddress;
        })
      )

      // .pipe(
      //   replace("%AdditionalLinks%", () => {
      //     if (additionalLinks.length >= 0) return "";
      //     return newEmailData.additionalLinks;
      //   })
      // )
      .pipe(dest(`${emailPathDest}/${newEmailData.emailname}/app/`))
  );
};
