const argv = require("yargs-parser")(process.argv.slice(2));
const PluginError = require("plugin-error");
const fs = require("fs");

const fetchParameter = (parameter) => {
  if (argv.hasOwnProperty(parameter)) {
    return argv[parameter];
  } else {
    throw new PluginError(parameter, `--${parameter} parameter not specified!`);
  }
};

const getFolder = () => {
  return fetchParameter("folder");
};

const getEmailName = () => {
  return fetchParameter("emailname");
};

const getPort = () => {
  return fetchParameter("port");
};

const getNewEmailParams = () => {
  const emailname = fetchParameter("emailname");
  const imageServerSubfolder = fetchParameter("imageSubfolder");
  const emailWidth = fetchParameter("emailWidth");
  const mainLink = fetchParameter("mainLink");
  const emailSubject = fetchParameter("emailSubject");
  const emailPreview = fetchParameter("emailPreview");
  const emailFromAddress = fetchParameter("emailFromAddress");
  // const vendor = fetchParameter("vendor");
  // const clicktag = fetchParameter("clicktag");

  return {
    emailname,
    imageServerSubfolder,
    emailWidth,
    mainLink,
    emailSubject,
    emailPreview,
    emailFromAddress,
  };
};

const getDocketParams = () => {
  const docketNumber = fetchParameter("docketnum");
  const projectName = fetchParameter("projectname");
  const imageServer = fetchParameter("imageServer");
  return {
    docketNumber,
    projectName,
    imageServer,
  };
};

const getClientParams = () => {
  const clientName = fetchParameter("clientname");
  const ftpDomain = fetchParameter("ftpdomain");
  return { clientName, ftpDomain };
};

function getEmailPath(folder, subpath) {
  let folderPath = `../emails/${folder}`;
  if (subpath) folderPath += `/${subpath}`;
  try {
    var stats = fs.lstatSync(folderPath);
    if (stats.isDirectory()) {
      return folderPath;
    }
  } catch (err) {
    throw new PluginError(
      "folder",
      `the folder path ${folderPath} does not exist!`
    );
  }
}

const getData = () => {
  return fetchParameter("data");
};

module.exports = {
  fetchParameter,
  getFolder,
  getEmailName,
  getPort,
  getDocketParams,
  getClientParams,
  getData,
  getNewEmailParams,
  getEmailPath,
};
