module.exports = function () {
  const fs = require("fs");
  const replace = require("gulp-string-replace");
  const PluginError = require("plugin-error");
  const { src, dest } = require("gulp");

  const getParams = require("./getParams");

  const folder = getParams.getFolder();
  const newDocketData = getParams.getDocketParams();
  const newFolderName = `${newDocketData.docketNumber} - ${newDocketData.projectName}`;

  if (fs.existsSync(`../banners/${folder}/${newFolderName}`)) {
    throw new PluginError(
      "docket",
      `the docket folder ${newFolderName} already exists!`
    );
  }

  let srcTemplate = ["templates/folderList/docket/**/*"];

  return src(srcTemplate, { allowEmpty: true })
    .pipe(
      replace("%CreateNewDocket%", () => {
        return newDocketData.docketNumber;
      })
    )
    .pipe(
      replace("%CreateNewProjectName%", () => {
        return newDocketData.projectName;
      })
    )
    .pipe(
      replace("%ImageServer%", () => {
        return newDocketData.imageServer;
      })
    )
    .pipe(dest(`../emails/${folder}/${newFolderName}`));
};
