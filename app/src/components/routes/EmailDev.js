import React, { useEffect } from "react";
import { Box, CssBaseline, Snackbar, IconButton } from "@material-ui/core";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import { FileCopy } from "@material-ui/icons";

import { Helmet } from "react-helmet";

import Hotkeys from "react-hot-keys";

import { useDispatch, useSelector } from "react-redux";

import clipboard from "copy-to-clipboard";

import { unslugify } from "../tools/folderTools";

import HeaderBar from "../tools/EmailHeaderBar";

import EmailViewFrame from "../tools/EmailViewFrame";
import EmailToolFrame from "../tools/EmailToolsFrame";
import TerminalFrame from "../tools/TerminalFrame";

import { startDev, refreshIFrame } from "../store/actions/IFrameActions";

import { drawerWidth, terminalHeight } from "../commonValues";

const useStyles = makeStyles((theme) => ({
  root: { display: "flex", height: "100%" },
  content: {
    display: "flex",
    flexFlow: "column",
    width: "100%",
    padding: theme.spacing(0),
    height: "100%",
    transition: theme.transitions.create(["margin", "height"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: 0,
  },
  contentShift: {
    transition: theme.transitions.create(["margin", "height"], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: drawerWidth,
  },
  terminalOpen: {
    transition: theme.transitions.create(["margin", "height"], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    height: `calc(100vh - ${terminalHeight}px)`,
    // marginBottom: terminalHeight
  },
  boxContent: {
    flexGrow: 1,
  },
  snackbar: {
    top: 70,
  },
}));

const EmailDev = (props) => {
  const { client, docket, emailfolder } = props.match.params;
  const clientSlug = unslugify(client);
  const docketSlug = unslugify(docket);
  const classes = useStyles();
  const dispatch = useDispatch();

  const {
    drawerOpen,
    emailReady,
    ngrokRunning,
    ngrokProcessing,
    ngrokLocation,
  } = useSelector((state) => ({
    ...state.toolsReducer,
    ...state.iFrameReducer,
    ...state.terminalReducer,
    ...state.ngrokReducer,
  }));

  // useEffect(() => {
  //   const fullpath = `${clientSlug}/${docketSlug}/${emailfolder}`;
  //   dispatch(startDev(fullpath, emailfolder));

  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, []);

  // // const onKeyUp = (keyName, e, handle) => {
  // //   console.log("key is up");
  // //   console.log(emailReady);
  // //   if (emailReady) dispatch(refreshIFrame());
  // // };

  const copyNgrokUrl = (e, reason) => {
    console.log("copyNgrokUrl Invoked");
    console.log(ngrokLocation);
    clipboard(ngrokLocation);
  };

  return (
    <div className={classes.root}>
      <Helmet>
        <title>{emailfolder}</title>
      </Helmet>
      <CssBaseline />
      <Hotkeys
        keyName="option+r"
        onKeyUp={(keyName, e, handle) => {
          console.log("key is up");
          console.log(emailReady);
          if (emailReady) dispatch(refreshIFrame());
        }}
      >
        <main
          className={clsx(classes.content, {
            [classes.contentShift]: drawerOpen,
            // [classes.terminalOpen]: terminalTrayOpen
          })}
        >
          <HeaderBar
            title={emailfolder}
            client={clientSlug}
            docket={docketSlug}
          />

          <Snackbar
            className={classes.snackbar}
            anchorOrigin={{ vertical: "top", horizontal: "right" }}
            open={ngrokProcessing}
            message="Starting Ngrok"
          />
          <Snackbar
            className={classes.snackbar}
            anchorOrigin={{ vertical: "top", horizontal: "right" }}
            open={ngrokRunning}
            message={<span>Sharing at: {ngrokLocation}</span>}
            action={[
              <IconButton
                key="copy"
                aria-label="copy"
                color="inherit"
                onClick={copyNgrokUrl}
              >
                <FileCopy />
              </IconButton>,
            ]}
          />

          <Box
            // height="100%"
            width="100%"
            overflow="hidden"
            className={classes.boxContent}
          >
            <EmailViewFrame />
          </Box>
          <TerminalFrame />
        </main>
        <EmailToolFrame />
      </Hotkeys>
    </div>
  );
};

export default EmailDev;
