import React, {
  useEffect,
  Fragment,
  useState,
  useCallback,
  useMemo,
} from "react";
import { useDispatch, useSelector } from "react-redux";

import { Helmet } from "react-helmet";

import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  IconButton,
  Checkbox,
  Paper,
  Typography,
  Toolbar,
  Fab,
  TextField,
  InputAdornment,
} from "@material-ui/core";
import { Link as MaterialLink } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { Folder, Add, Refresh, FilterList, Clear } from "@material-ui/icons";
import { Link } from "react-router-dom";

import AddNewEmailDialog from "../tools/listingDialogBoxes/AddNewEmailDialog";
import AddNewDocketDialog from "../tools/listingDialogBoxes/AddNewDocketDialog";
import AddNewClientDialog from "../tools/listingDialogBoxes/AddNewClientDialog";

import BatchProcessingDialogBox from "../tools/listingDialogBoxes/BatchProcessingDialogBox";

import { slugify, unslugify } from "../tools/folderTools";

import FolderHeaderBar from "../tools/FolderHeaderBar";

// import { resetBannerDev } from "../store/actions/IFrameActions";
import {
  getClientList,
  getDocketList,
  getEmailList,
  addSelected,
  setSelected,
  resetSelected,
  updateFilter,
} from "../store/actions/EmailListActions";

import { getFolderLevel } from "../tools/folderTools";

import EmailListingSelectTools from "../tools/listingSelectTools/EmailListingSelectTools";
import ClientListingSelectTools from "../tools/listingSelectTools/ClientListingSelectTools";
import DocketListingSelectTools from "../tools/listingSelectTools/DocketListingSelectTools";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: "100%",
  },
  table: {
    minWidth: 650,
  },
  rootGrid: {
    padding: theme.spacing(2),
  },
  paper: {
    marginTop: theme.spacing(3),
    width: "100%",
    overflowX: "auto",
    marginBottom: theme.spacing(2),
  },
  icon: {
    marginRight: theme.spacing(2),
  },
  title: {
    flex: "1",
  },
  fabBox: {
    position: "absolute",
    bottom: theme.spacing(2),
    right: theme.spacing(4),
  },
  fab: {
    margin: theme.spacing(1),
  },
  filterInput: {
    marginLeft: theme.spacing(2),
  },
}));

const EmailListing = (props) => {
  const [addDialogOpen, setAddDialogOpen] = useState(false);
  //   // const [editDialogOpen, setEditDialogOpen] = useState(false);

  const dispatch = useDispatch();

  const { client, docket } = props.match.params;

  const { clients, dockets, banners, selected, ftplink, filter } = useSelector(
    (state) => ({
      ...state.emailListReducer,
    })
  );
  const folderLevel = getFolderLevel(client, docket);

  //   useEffect(() => {
  //     dispatch(resetBannerDev());
  //     // dispatch(getClientList());
  //     // eslint-disable-next-line react-hooks/exhaustive-deps
  //   }, []);

  const refreshFolders = useCallback(() => {
    switch (folderLevel) {
      case 0:
        dispatch(getClientList());
        break;
      case 1:
        dispatch(getDocketList(client));
        break;
      case 2:
        dispatch(getEmailList(client, docket));
        break;
      default:
        dispatch(getClientList());
        break;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [client, docket, folderLevel]);

  useEffect(() => {
    refreshFolders();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [client, docket, folderLevel]);

  const classes = useStyles();

  const [selectAllBool, setSelectAllBool] = useState(false);
  const [indeterminateBool, setIndeterminateBool] = useState(false);

  const handleSelectAllClick = (event) => {
    const newSelectAllBool = !selectAllBool;

    if (newSelectAllBool) {
      const newSelected = rowData.map((n) => n.name);
      dispatch(setSelected(newSelected));
    } else {
      dispatch(setSelected([]));
    }
    setSelectAllBool(newSelectAllBool);
  };

  const handleSelectItem = (event, name) => {
    console.log("item selected");
    dispatch(addSelected(name));
  };

  const createTitle = (client, docket) => {
    let newTitle = "Rethink HTML Email Machine";
    if (typeof client !== "undefined") {
      newTitle = unslugify(client);
    }
    if (typeof docket !== "undefined") {
      newTitle = unslugify(docket);
    }
    return (
      <Helmet>
        <title>{newTitle}</title>
      </Helmet>
    );
  };

  const createRowData = (clients, dockets, banners) => {
    let rowData = [];
    const regexp = new RegExp(filter, "i");
    if (folderLevel === 0) {
      rowData = clients
        .filter((name) => regexp.test(name))
        .map((name) => {
          return { name, link: `/list/${slugify(name)}` };
        });
    } else if (folderLevel === 1) {
      rowData = dockets
        .filter((name) => regexp.test(name))
        .map((name) => {
          return { name, link: `/list/${slugify(client)}/${slugify(name)}` };
        });
    } else if (folderLevel === 2) {
      rowData = banners
        .filter((name) => regexp.test(name))
        .map((name) => {
          return {
            name,
            link: `/dev/${slugify(client)}/${slugify(docket)}/${name}`,
          };
        });
    }
    return rowData;
  };

  const rowData = createRowData(clients, dockets, banners);

  const isSelected = (name) => selected.indexOf(name) !== -1;

  useEffect(() => {
    setIndeterminateBool(
      selected.length > 0 && selected.length < rowData.length
    );
    if (rowData.length <= 0) {
      setSelectAllBool(false);
    } else {
      setSelectAllBool(selected.length === rowData.length);
    }
  }, [selected, rowData]);

  const createRows = (rowData) => {
    return rowData.map((row, keyIndex) => {
      const isItemSelected = isSelected(row.name);
      return (
        <TableRow hover key={keyIndex}>
          <TableCell padding="checkbox">
            <Checkbox
              checked={isItemSelected}
              onClick={(e) => handleSelectItem(e, row.name)}
            />
          </TableCell>
          <TableCell
            compohnent="th"
            scope="row"
            align="left"
            padding="checkbox"
          >
            <IconButton
              color="inherit"
              className={classes.icon}
              component={Link}
              onClick={(e) => {
                handleCloseDialog();
                dispatch(resetSelected());
              }}
              to={row.link}
            >
              <Folder />
            </IconButton>
          </TableCell>
          <TableCell align="left">{row.name}</TableCell>
        </TableRow>
      );
    });
  };

  const memoEnhancedTableToolbar = useMemo(() => {
    //     // const { numSelected, folderLevel } = props;
    const numSelected = selected.length;
    let levelLabel = "";
    let SelectionTools;
    switch (folderLevel) {
      case 0:
        levelLabel = "Client";
        SelectionTools = ClientListingSelectTools;
        break;
      case 1:
        levelLabel = "Docket";
        SelectionTools = DocketListingSelectTools;
        break;
      case 2:
        levelLabel = "Banner";
        SelectionTools = EmailListingSelectTools;
        break;
      default:
        break;
    }

    return (
      <Toolbar>
        {numSelected > 0 ? (
          <Fragment>
            <Typography
              className={classes.title}
              color="inherit"
              variant="subtitle1"
            >
              {numSelected} {levelLabel}
              {numSelected > 1 ? "s" : ""} selected
            </Typography>
            <SelectionTools client={client} docket={docket} />
          </Fragment>
        ) : (
          <Typography className={classes.title} variant="h6" id="tableTitle">
            {levelLabel}s{" "}
          </Typography>
        )}
        <TextField
          className={classes.filterInput}
          value={filter}
          key="folderFilterInput"
          onChange={(e) => {
            dispatch(updateFilter(e.currentTarget.value));
          }}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <FilterList />
              </InputAdornment>
            ),
            endAdornment: (
              <InputAdornment position="end">
                <IconButton
                  onClick={(e) => {
                    dispatch(updateFilter(""));
                  }}
                >
                  <Clear />
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
      </Toolbar>
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selected, folderLevel, filter]);

  const handleCloseDialog = () => {
    setAddDialogOpen(false);
    // setEditDialogOpen(false);
  };

  const renderFabButtons = () => {
    switch (folderLevel) {
      case 0:
        return (
          <AddNewClientDialog
            open={addDialogOpen}
            onClose={handleCloseDialog}
          />
        );
      case 1:
        return (
          <AddNewDocketDialog
            client={unslugify(client)}
            open={addDialogOpen}
            onClose={handleCloseDialog}
          />
        );
      case 2:
        return (
          <AddNewEmailDialog
            client={unslugify(client)}
            docket={unslugify(docket)}
            open={addDialogOpen}
            onClose={handleCloseDialog}
          />
        );

      default:
        return <div></div>;
    }
  };

  return (
    <div className={classes.root}>
      {createTitle(client, docket)}

      <FolderHeaderBar
        client={client}
        docket={docket}
        folderLevel={folderLevel}
      />
      <Paper className={classes.paper}>
        {memoEnhancedTableToolbar}

        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              <TableCell padding="checkbox">
                <Checkbox
                  indeterminate={indeterminateBool}
                  checked={selectAllBool}
                  onChange={handleSelectAllClick}
                />
              </TableCell>
              <TableCell align="left" padding="checkbox">
                <IconButton
                  color="inherit"
                  className={classes.icon}
                  onClick={(e) => {
                    refreshFolders();
                  }}
                >
                  <Refresh />
                </IconButton>
              </TableCell>
              <TableCell className={classes.folderCell} align="left">
                <b>Folder</b>{" "}
                {folderLevel === 2 && ftplink !== "" && (
                  <i>
                    / Staging :{" "}
                    <MaterialLink href={ftplink} target="_blank" rel="noopener">
                      {ftplink}
                    </MaterialLink>
                  </i>
                )}
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>{createRows(rowData)}</TableBody>
        </Table>
      </Paper>
      <div className={classes.fabBox}>
        <Fab
          color="primary"
          className={classes.fab}
          onClick={() => {
            setAddDialogOpen(true);
          }}
        >
          <Add />
        </Fab>
      </div>
      {renderFabButtons()}
      <BatchProcessingDialogBox />
    </div>
  );
};

export default EmailListing;
