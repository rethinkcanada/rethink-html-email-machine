import React, { useEffect, useRef, useCallback } from "react";
import Iframe from "react-iframe";
import { useSelector, useDispatch } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import { CircularProgress } from "@material-ui/core";

import { saveStatic } from "../store/actions/BuildActions";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    position: "relative",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  child: {
    display: "block",
    position: "absolute",
  },
}));

const BannerViewFrame = () => {
  const dispatch = useDispatch();

  const classes = useStyles();

  const {
    iframeRefCount,
    emailURL,
    emailfolder,
    emailname,
    // staticProcessing
  } = useSelector((state) => ({
    ...state.iFrameReducer,
    ...state.buildReducer,
  }));

  const iFrameRef = useRef(null);

  const postHandler = useCallback(
    (e) => {
      if (e.origin === emailURL) {
        console.log("emailfolder", emailfolder);
        const messageData = JSON.parse(e.data);
        switch (messageData.event) {
          case "createScreenshotDone":
            dispatch(saveStatic(emailfolder, emailname, messageData.data));
            break;
          default:
            break;
        }
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [emailURL, emailfolder, dispatch]
  );

  useEffect(() => {
    window.addEventListener("message", postHandler, false);

    return () => {
      window.removeEventListener("message", postHandler);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [postHandler]);

  useEffect(() => {
    if (emailURL !== "") {
      iFrameRef.current = document.getElementById("MainIFrame").contentWindow;
    }
    // if (staticProcessing && iFrameRef.current) {
    //   iFrameRef.current.postMessage("createScreenshot", emailURL);
    // }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // console.log(`${emailURL}?var=${randomVar}`);

  return (
    <div className={classes.root}>
      {emailURL !== "" ? (
        <Iframe
          url={emailURL}
          width="100%"
          height="100%"
          position="absolute"
          frameBorder="0"
          id="MainIFrame"
          sandbox="allow-same-origin allow-scripts allow-popups"
          key={iframeRefCount}
        />
      ) : (
        <CircularProgress className={classes.child} />
      )}
    </div>
  );
};

export default BannerViewFrame;
