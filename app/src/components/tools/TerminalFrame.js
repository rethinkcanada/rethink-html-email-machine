import React, { useRef, useEffect } from "react";
import clsx from "clsx";
import { useDispatch, useSelector } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import {
  AppBar,
  Toolbar,
  Typography,
  IconButton,
  Tooltip
} from "@material-ui/core";
import {
  ExpandLess,
  ExpandMore,
  GetApp,
  GetAppOutlined
} from "@material-ui/icons";
import { Console } from "console-feed";

import { terminalHeight } from "../commonValues";

import {
  toggleTerminal,
  toggleTerminalAutoscrolling
} from "../store/actions/terminalActions";

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    height: "auto",
    bottom: 0
  },
  terminalBarOpen: {
    height: terminalHeight + "px !important",
    transition: theme.transitions.create(["height"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    })
  },
  title: {
    flexGrow: 1,
    textAlign: "left"
  },
  appBar: {
    top: 0
  },
  scrollWrapper: {
    textAlign: "left",
    display: "block",
    width: "100%",
    height: "0px",
    overflowY: "auto",
    backgroundColor: "#434855",
    transition: theme.transitions.create(["height"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    })
  }
}));

// const TerminalScrollWrapper = styled.div`
//   display: block;
//   width: 100%;
//   height: 100%;
//   overflow-y: auto;
//   background-color: #434855;
// `;

// const TerminalBody = styled.div`
//   display: block;
//   width: 100%;
//   height: 100%;
//   background-color: #434855;
// `;

//TODO: add a clear terminal option

const TerminalFrame = props => {
  const {
    logs,
    terminalAutoScrolling,
    drawerOpen,
    terminalTrayOpen
  } = useSelector(state => ({
    ...state.terminalReducer,
    ...state.toolsReducer
  }));
  const dispatch = useDispatch();

  const classes = useStyles();

  const scrollRef = useRef();

  const getToggleIcon = trayOpen => {
    if (trayOpen) {
      return <ExpandMore />;
    } else {
      return <ExpandLess />;
    }
  };

  const getAutoScrollToggleIcon = autoScrolling => {
    if (autoScrolling) {
      return <GetApp />;
    } else {
      return <GetAppOutlined />;
    }
  };

  useEffect(() => {
    if (terminalAutoScrolling)
      scrollRef.current.scrollTop = scrollRef.current.scrollHeight;
  }, [logs, terminalAutoScrolling]);

  return (
    <div
      className={clsx(classes.root, {
        [classes.appBarShift]: drawerOpen
      })}
    >
      <AppBar position="relative" className={classes.appBar}>
        <Toolbar variant="dense">
          <Typography variant="body2" color="inherit" className={classes.title}>
            Terminal
          </Typography>
          <Tooltip title="Auto Scrolling">
            <IconButton
              edge="end"
              color="inherit"
              aria-label="Terminal"
              onClick={() => {
                dispatch(toggleTerminalAutoscrolling());
              }}
            >
              {getAutoScrollToggleIcon(terminalAutoScrolling)}
            </IconButton>
          </Tooltip>
          <IconButton
            edge="end"
            color="inherit"
            aria-label="Terminal"
            onClick={() => {
              dispatch(toggleTerminal());
            }}
          >
            {getToggleIcon(terminalTrayOpen)}
          </IconButton>
        </Toolbar>

        <div
          className={clsx(classes.scrollWrapper, {
            [classes.terminalBarOpen]: terminalTrayOpen
          })}
          ref={scrollRef}
        >
          <Console logs={logs} variant="dark" />
        </div>

        {/* <Drawer
          // classes={{ paper: classes.drawerPaper }}
          anchor="bottom"
          variant="persistent"
          open={true}
        >
          hello wolrd
        </Drawer> */}
      </AppBar>

      {/* <ExpansionPanel className={classes.scrollWrapper} ref={scrollRef}> */}

      {/* </ExpansionPanel> */}
    </div>
  );
};

export default TerminalFrame;
