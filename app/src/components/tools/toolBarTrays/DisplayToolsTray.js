import React from "react";

import { useDispatch, useSelector } from "react-redux";

import { ListItem, ListItemIcon, ListItemText } from "@material-ui/core";
import { RefreshRounded } from "@material-ui/icons";

import { refreshIFrame } from "../../store/actions/IFrameActions";

const DisplayToolTray = () => {
  const dispatch = useDispatch();

  const { bannerReady } = useSelector(state => ({
    ...state.iFrameReducer
  }));

  return (
    <ListItem
      button
      disabled={!bannerReady}
      onClick={() => {
        dispatch(refreshIFrame());
      }}
    >
      <ListItemIcon>
        <RefreshRounded />
      </ListItemIcon>
      <ListItemText primary="Refresh Banner"></ListItemText>
    </ListItem>
  );
};

export default DisplayToolTray;
