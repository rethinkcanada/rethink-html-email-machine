module.exports = {
  getFolderLevel: function(client, docket) {
    if (typeof client === "undefined" && typeof docket === "undefined") {
      return 0;
    } else if (typeof client !== "undefined" && typeof docket === "undefined") {
      return 1;
    } else if (typeof client !== "undefined" && typeof docket !== "undefined") {
      return 2;
    } else {
      return 0;
    }
  },
  slugify: function(folder) {
    return folder.split(" ").join("_");
  },
  unslugify: function(folder) {
    return folder.split("_").join(" ");
  }
};
