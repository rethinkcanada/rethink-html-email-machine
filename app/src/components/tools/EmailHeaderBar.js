import React from "react";
import { useDispatch } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import { AppBar, Toolbar, Typography, IconButton } from "@material-ui/core";
import { CancelRounded, MenuRounded } from "@material-ui/icons";
import { Link } from "react-router-dom";

import { slugify } from "../tools/folderTools";

import { toggleDrawer } from "../store/actions/toolsActions";
import { stopDev } from "../store/actions/IFrameActions";

const useStyles = makeStyles((theme) => ({
  title: {
    flexGrow: 1,
    textAlign: "center",
  },
  // appBar: {
  //   transition: theme.transitions.create(["margin", "width"], {
  //     easing: theme.transitions.easing.sharp,
  //     duration: theme.transitions.duration.leavingScreen
  //   })
  // },
  // appBarShift: {
  //   // width: `calc(100% - ${drawerWidth}px)`,
  //   // marginLeft: drawerWidth,
  //   transition: theme.transitions.create(["margin", "width"], {
  //     easing: theme.transitions.easing.easeOut,
  //     duration: theme.transitions.duration.enteringScreen
  //   })
  // },
  backButton: {
    // marginLeft: theme.spacing(2)
  },
}));

const BannerHeaderBar = (props) => {
  const dispatch = useDispatch();

  const { title, client, docket } = props;

  // const { drawerOpen } = useSelector(state => ({
  //   ...state.iFrameReducer,
  //   ...state.toolsReducer
  // }));

  const classes = useStyles();

  return (
    <AppBar
      position="relative"
      // className={clsx(classes.appBar, {
      //   [classes.appBarShift]: drawerOpen
      // })}
    >
      <Toolbar>
        <IconButton
          edge="start"
          color="inherit"
          aria-label="Menu"
          onClick={() => {
            dispatch(toggleDrawer());
          }}
        >
          <MenuRounded />
        </IconButton>
        <Typography variant="h5" color="inherit" className={classes.title}>
          {title}
        </Typography>
        <IconButton
          component={Link}
          edge="end"
          color="inherit"
          aria-label="Back"
          className={classes.backButton}
          to={`/list/${slugify(client)}/${slugify(docket)}`}
          onClick={() => {
            dispatch(stopDev());
          }}
        >
          <CancelRounded />
        </IconButton>
      </Toolbar>
    </AppBar>
  );
};

export default BannerHeaderBar;
