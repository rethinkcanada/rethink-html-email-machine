import React from "react";
import { useDispatch } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import {
  AppBar,
  Toolbar,
  Typography,
  Breadcrumbs,
  Link,
} from "@material-ui/core";

import LinkRouter from "react-router-dom/Link";
import { resetSelected } from "../store/actions/EmailListActions";

import { slugify, unslugify } from "../tools/folderTools";

const useStyles = makeStyles((theme) => ({
  backButton: {
    marginLeft: theme.spacing(2),
  },
  paper: {
    padding: theme.spacing(1, 2),
  },
  root: {
    justifyContent: "left",
    // flexWrap: "wrap"
  },
}));

const FolderHeaderBar = ({ client, docket, folderLevel }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  if (typeof docket !== "undefined") docket = unslugify(docket);
  if (typeof client !== "undefined") client = unslugify(client);

  const renderCrumbFolder = (folder, linkTarget) => {
    if (linkTarget) {
      return (
        <Link
          color="inherit"
          component={LinkRouter}
          to={linkTarget}
          onClick={(e) => dispatch(resetSelected())}
        >
          {folder}
        </Link>
      );
    } else {
      return <Typography>{folder}</Typography>;
    }
  };

  const makeFolderHierarchy = (client, docket, folderLevel) => {
    switch (folderLevel) {
      case 0:
        return (
          <Breadcrumbs separator="›" color="inherit">
            {renderCrumbFolder("Rethink HTML Email Machine")}
          </Breadcrumbs>
        );
      case 1:
        return (
          <Breadcrumbs separator="›" color="inherit">
            {renderCrumbFolder("Rethink HTML Email Machine", "/list/")}
            {renderCrumbFolder(client)}
          </Breadcrumbs>
        );
      case 2:
        return (
          <Breadcrumbs separator="›" color="inherit">
            {renderCrumbFolder("Rethink HTML Email Machine", "/list/")}
            {renderCrumbFolder(client, `/list/${slugify(client)}`)}
            {renderCrumbFolder(docket)}
          </Breadcrumbs>
        );
      default:
        break;
    }
  };

  return (
    <AppBar position="relative">
      <Toolbar className={classes.root}>
        {makeFolderHierarchy(client, docket, folderLevel)}
      </Toolbar>
    </AppBar>
  );
};

export default FolderHeaderBar;
