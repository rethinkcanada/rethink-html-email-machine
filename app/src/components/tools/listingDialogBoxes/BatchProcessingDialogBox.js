import React from "react";

import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
  CircularProgress,
  Grid,
  Typography,
  Icon,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { green, red } from "@material-ui/core/colors";
import { Done, Error } from "@material-ui/icons";
import { useDispatch, useSelector } from "react-redux";

import { resetBatchProcess } from "../../store/actions/BatchProcessActions";

const useStyles = makeStyles((theme) => ({
  formContainer: {
    display: "flex",
    flexWrap: "wrap",
  },
  iconSuccess: {
    color: green[500],
  },
  iconError: {
    color: red[500],
  },
  buttonProgress: {
    color: green[500],
    position: "absolute",
    top: "50%",
    left: "50%",
    marginTop: -12,
    marginLeft: -12,
  },
  buttonWrapper: {
    margin: theme.spacing(1),
    position: "relative",
  },
}));

const AddNewDialogBox = (props) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const {
    batchProcessType,
    batchProcessing,
    // batchComplete,
    batchProcessList,
    batchProcessDialogOpen,
  } = useSelector((state) => ({
    ...state.batchProcessReducer,
  }));

  // const buttonClassname = clsx({
  //   [classes.buttonSuccess]: successFlag,
  //   [classes.buttonError]: errorFlag
  // });

  const renderProcessStatus = (status) => {
    switch (status) {
      case "processing":
      default:
        return (
          <Icon>
            <CircularProgress size={17} />
          </Icon>
        );
      case "complete":
        return (
          <Icon className={classes.iconSuccess}>
            <Done />
          </Icon>
        );
      case "error":
        return (
          <Icon className={classes.iconError}>
            <Error />
          </Icon>
        );
    }
  };

  return (
    <Dialog
      open={batchProcessDialogOpen}
      onClose={(e) => dispatch(resetBatchProcess())}
      disableBackdropClick={batchProcessing}
      fullWidth
      maxWidth="lg"
    >
      <DialogTitle>Batch Processing: {batchProcessType}</DialogTitle>
      <DialogContent dividers>
        {batchProcessList.map((banner) => {
          return (
            <Grid container wrap="nowrap" spacing={4}>
              <Grid item>{renderProcessStatus(banner.status)}</Grid>
              <Grid item>
                <Typography>{banner.name}</Typography>
              </Grid>
            </Grid>
          );
        })}
      </DialogContent>
      <DialogActions>
        <div className={classes.buttonWrapper}>
          <Button
            color="primary"
            variant="contained"
            disabled={batchProcessing}
            onClick={(e) => dispatch(resetBatchProcess())}
          >
            Close
          </Button>
          {batchProcessing && (
            <CircularProgress size={24} className={classes.buttonProgress} />
          )}
        </div>
      </DialogActions>
    </Dialog>
  );
};

export default AddNewDialogBox;
