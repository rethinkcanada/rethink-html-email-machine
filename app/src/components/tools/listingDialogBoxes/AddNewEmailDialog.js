import React, { useState, useEffect } from "react";
import {
  TextField,
  InputLabel,
  Select,
  MenuItem,
  FormControl,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { useDispatch, useSelector } from "react-redux";

import AddNewDialogBox from "./AddNewDialogBox";

import {
  createEmail,
  resetCreateEdit,
} from "../../store/actions/CreateEditActions";
import { getEmailList } from "../../store/actions/EmailListActions";

const useStyles = makeStyles((theme) => ({
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    flexGrow: "1",
  },
  select: {
    marginTop: theme.spacing(1),
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    flexGrow: "1",
  },
}));

const AddNewEmailDialog = (props) => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const { open, onClose, client, docket } = props;
  const folder = `${client}/${docket}`;

  const {
    createEditProcessing,
    createEditSuccess,
    createEditError,
    createEditErrorMessage,
  } = useSelector((state) => ({
    ...state.createEditReducer,
  }));

  const [emailname, setEmailname] = useState("");
  const [imageSubfolder, setImageSubfolder] = useState("");
  const [emailWidth, setEmailWidth] = useState(659);
  const [emailSubject, setEmailSubject] = useState("");
  const [emailPreview, setEmailPreview] = useState("");
  const [emailFromAddress, setEmailFromAddress] = useState("");
  const [mainLink, setMainLink] = useState("https://www.google.com");
  const [valid, setValid] = useState(false);

  useEffect(() => {
    if (
      emailname === "" ||
      imageSubfolder === "" ||
      emailWidth === "" ||
      emailSubject === "" ||
      emailPreview === "" ||
      emailFromAddress === "" ||
      mainLink === ""
    ) {
      setValid(false);
    } else {
      setValid(true);
    }
  }, [
    emailname,
    imageSubfolder,
    emailWidth,
    emailSubject,
    emailPreview,
    emailFromAddress,
    mainLink,
  ]);

  useEffect(() => {
    if (createEditSuccess) {
      dispatch(getEmailList(client, docket));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [createEditSuccess, client, docket]);

  const handleClose = () => {
    onClose();
    dispatch(resetCreateEdit());
    setEmailname("");
    setImageSubfolder("");
    setEmailWidth(659);
    setEmailSubject("");
    setEmailPreview("");
    setEmailFromAddress("");
    setMainLink("");
  };
  const handleCreate = () => {
    if (valid) {
      dispatch(
        createEmail({
          folder,
          imageSubfolder,
          emailWidth,
          emailname,
          mainLink,
          emailSubject,
          emailPreview,
          emailFromAddress,
        })
      );
    }
  };

  return (
    <AddNewDialogBox
      dialogTitle="Add a new Email"
      open={open}
      onClose={onClose}
      errorFlag={createEditError}
      errorMsg={createEditErrorMessage}
      successFlag={createEditSuccess}
      handleClose={handleClose}
      handleCreate={handleCreate}
      processing={createEditProcessing}
      valid={valid}
    >
      <TextField
        label="Email name"
        margin="normal"
        variant="outlined"
        autoFocus
        onChange={(e) => setEmailname(e.target.value)}
        disabled={createEditProcessing}
        value={emailname}
        style={{ margin: 8, flexGrow: 2 }}
      />
      <TextField
        label="Width"
        type="number"
        onChange={(e) => setEmailWidth(e.target.value)}
        disabled={createEditProcessing}
        value={emailWidth}
        margin="normal"
        variant="outlined"
        className={classes.textField}
        style={{ margin: 8, flexGrow: 2 }}
      />

      <TextField
        label="Image Server Sub Folder"
        value={imageSubfolder}
        onChange={(e) => setImageSubfolder(e.target.value)}
        disabled={createEditProcessing}
        margin="normal"
        variant="outlined"
        style={{ margin: 8 }}
        fullWidth
      />

      <TextField
        label="Subject Text"
        value={emailSubject}
        onChange={(e) => setEmailSubject(e.target.value)}
        disabled={createEditProcessing}
        margin="normal"
        variant="outlined"
        style={{ margin: 8 }}
        fullWidth
      />

      <TextField
        label="Preview Text"
        value={emailPreview}
        onChange={(e) => setEmailPreview(e.target.value)}
        disabled={createEditProcessing}
        margin="normal"
        variant="outlined"
        style={{ margin: 8 }}
        fullWidth
      />

      <TextField
        label="From Email"
        value={emailFromAddress}
        onChange={(e) => setEmailFromAddress(e.target.value)}
        disabled={createEditProcessing}
        margin="normal"
        variant="outlined"
        style={{ margin: 8 }}
        fullWidth
      />

      <TextField
        label="Main Link"
        value={mainLink}
        onChange={(e) => setMainLink(e.target.value)}
        disabled={createEditProcessing}
        margin="normal"
        variant="outlined"
        style={{ margin: 8 }}
        fullWidth
      />
    </AddNewDialogBox>
  );
};

export default AddNewEmailDialog;
