import React, { useState, useEffect } from "react";
import { TextField } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { useDispatch, useSelector } from "react-redux";

import AddNewDialogBox from "./AddNewDialogBox";

import {
  createDocket,
  resetCreateEdit,
} from "../../store/actions/CreateEditActions";
import { getDocketList } from "../../store/actions/EmailListActions";

const useStyles = makeStyles((theme) => ({
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    flexGrow: "1",
  },
  select: {
    marginTop: theme.spacing(1),
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    flexGrow: "1",
  },
}));

const AddNewDocketDialog = (props) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { open, onClose, client } = props;
  const folder = `${client}`;

  const [docketNumber, setDocketNumber] = useState("");
  const [projectName, setProjectName] = useState("");
  const [imageServer, setImageServer] = useState("");

  const {
    createEditProcessing,
    createEditSuccess,
    createEditError,
    createEditErrorMessage,
  } = useSelector((state) => ({
    ...state.createEditReducer,
  }));

  const [valid, setValid] = useState(false);

  useEffect(() => {
    if (docketNumber === "" || projectName === "" || imageServer === "") {
      setValid(false);
    } else {
      setValid(true);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [docketNumber, projectName, imageServer]);

  useEffect(() => {
    if (createEditSuccess) {
      dispatch(getDocketList(client));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [createEditSuccess, client]);

  const handleClose = () => {
    onClose();
    dispatch(resetCreateEdit());
    setDocketNumber("");
    setProjectName("");
    setImageServer("");
  };

  const handleCreate = () => {
    if (valid) {
      dispatch(
        createDocket({ folder, docketNumber, projectName, imageServer })
      );
    }
  };

  return (
    <AddNewDialogBox
      dialogTitle="Add a new Docket folder"
      open={open}
      onClose={onClose}
      errorFlag={createEditError}
      errorMsg={createEditErrorMessage}
      successFlag={createEditSuccess}
      handleClose={handleClose}
      handleCreate={handleCreate}
      processing={createEditProcessing}
      valid={valid}
    >
      <TextField
        label="Docket"
        margin="normal"
        variant="outlined"
        autoFocus
        onChange={(e) => setDocketNumber(e.target.value)}
        value={docketNumber}
        className={classes.textField}
      />
      <TextField
        label="Project Name"
        margin="normal"
        variant="outlined"
        onChange={(e) => setProjectName(e.target.value)}
        value={projectName}
        className={classes.textField}
      />
      <TextField
        label="Image Server"
        margin="normal"
        variant="outlined"
        onChange={(e) => setProjectName(e.target.value)}
        value={projectName}
        className={classes.textField}
      />
    </AddNewDialogBox>
  );
};

export default AddNewDocketDialog;
