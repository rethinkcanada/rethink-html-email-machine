import React, { useState, useEffect } from "react";
import { TextField } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { useDispatch, useSelector } from "react-redux";

import AddNewDialogBox from "./AddNewDialogBox";

import {
  createClient,
  resetCreateEdit,
} from "../../store/actions/CreateEditActions";
import { getClientList } from "../../store/actions/EmailListActions";

const useStyles = makeStyles((theme) => ({
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    flexGrow: "1",
  },
  select: {
    marginTop: theme.spacing(1),
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    flexGrow: "1",
  },
}));

const AddNewDocketDialog = (props) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { open, onClose } = props;

  const [clientName, setClientName] = useState("");
  const [ftpDomain, setFTPDomain] = useState("");

  const {
    createEditProcessing,
    createEditSuccess,
    createEditError,
    createEditErrorMessage,
  } = useSelector((state) => ({
    ...state.createEditReducer,
  }));

  const [valid, setValid] = useState(false);

  useEffect(() => {
    if (clientName === "" || ftpDomain === "") {
      setValid(false);
    } else {
      setValid(true);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [clientName, ftpDomain]);

  useEffect(() => {
    if (createEditSuccess) {
      dispatch(getClientList());
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [createEditSuccess]);

  const handleClose = () => {
    onClose();
    dispatch(resetCreateEdit());
    setClientName("");
    setFTPDomain("");
  };

  const handleCreate = () => {
    if (valid) {
      dispatch(createClient({ clientName, ftpDomain }));
    }
  };

  return (
    <AddNewDialogBox
      dialogTitle="Add a new Client folder"
      open={open}
      onClose={onClose}
      errorFlag={createEditError}
      errorMsg={createEditErrorMessage}
      successFlag={createEditSuccess}
      handleClose={handleClose}
      handleCreate={handleCreate}
      processing={createEditProcessing}
      valid={valid}
    >
      <TextField
        label="Client Name"
        margin="normal"
        variant="outlined"
        autoFocus
        fullWidth
        onChange={(e) => setClientName(e.target.value)}
        value={clientName}
        className={classes.textField}
      />
      <TextField
        label="Domain"
        margin="normal"
        variant="outlined"
        fullWidth
        onChange={(e) => setFTPDomain(e.target.value)}
        value={ftpDomain}
        className={classes.textField}
      />
    </AddNewDialogBox>
  );
};

export default AddNewDocketDialog;
