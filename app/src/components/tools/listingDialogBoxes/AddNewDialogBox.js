import React from "react";
import clsx from "clsx";
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
  CircularProgress
} from "@material-ui/core";
import { green, red } from "@material-ui/core/colors";
import { makeStyles } from "@material-ui/core/styles";
// import { useDispatch, useSelector } from "react-redux";

const useStyles = makeStyles(theme => ({
  formContainer: {
    display: "flex",
    flexWrap: "wrap"
  },
  buttonWrapper: {
    margin: theme.spacing(1),
    position: "relative"
  },
  buttonProgress: {
    color: green[500],
    position: "absolute",
    top: "50%",
    left: "50%",
    marginTop: -12,
    marginLeft: -12
  },
  buttonSuccess: {
    backgroundColor: green[500],
    "&:hover": {
      backgroundColor: green[700]
    }
  },
  buttonError: {
    backgroundColor: red[500],
    "&:hover": {
      backgroundColor: red[700]
    }
  },
  errorMsg: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    padding: 14,
    backgroundColor: red[100],
    borderRadius: 10,
    "& p": {
      margin: 0
    }
  }
}));

const AddNewDialogBox = props => {
  const classes = useStyles();
  const {
    dialogTitle,
    open,
    onClose,
    errorFlag,
    errorMsg,
    successFlag,
    handleClose,
    handleCreate,
    processing,
    valid
  } = props;

  const buttonClassname = clsx({
    [classes.buttonSuccess]: successFlag,
    [classes.buttonError]: errorFlag
  });

  return (
    <Dialog open={open} onClose={onClose} disableBackdropClick={processing}>
      <DialogTitle>{dialogTitle}</DialogTitle>
      <DialogContent dividers>
        {errorFlag && (
          <DialogContentText className={classes.errorMsg}>
            {errorMsg.split("\n").map((item, i) => (
              <p key={i}>{item}</p>
            ))}
          </DialogContentText>
        )}
        <div className={classes.formContainer}>{props.children}</div>
      </DialogContent>
      <DialogActions>
        <Button color="primary" onClick={handleClose} disabled={processing}>
          Done
        </Button>
        <div className={classes.buttonWrapper}>
          <Button
            color="primary"
            variant="contained"
            className={buttonClassname}
            disabled={processing || !valid}
            onClick={handleCreate}
          >
            Create
          </Button>
          {processing && (
            <CircularProgress size={24} className={classes.buttonProgress} />
          )}
        </div>
      </DialogActions>
    </Dialog>
  );
};

export default AddNewDialogBox;
