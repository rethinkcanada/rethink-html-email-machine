import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { useDispatch } from "react-redux";
import { Drawer, List, IconButton, Divider } from "@material-ui/core";
import { ChevronLeft } from "@material-ui/icons";

import { useSelector } from "react-redux";

import DisplayToolsTray from "./toolBarTrays/DisplayToolsTray";
// import BuildToolsTray from "./toolBarTrays/BuildToolsTray";
// import ValidationToolsTray from "./toolBarTrays/ValidationToolsTray";
// import MediaBuyTray from "./toolBarTrays/MediaBuyTray";
// import NgrokToolsTray from "./toolBarTrays/NgrokToolTray";
// import FTPUploadToolsTray from "./toolBarTrays/FTPUploadToolsTray";

import { drawerWidth } from "../commonValues";

import { closeDrawer } from "../store/actions/toolsActions";

const useStyles = makeStyles((theme) => ({
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerHeader: {
    display: "flex",
    alignItems: "center",
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
    justifyContent: "flex-end",
  },
}));

const EmailToolsFrame = (props) => {
  const classes = useStyles();
  const distpatch = useDispatch();
  const { drawerOpen } = useSelector((state) => ({ ...state.toolsReducer }));
  return (
    <Drawer
      anchor="left"
      variant="persistent"
      open={drawerOpen}
      classes={{
        paper: classes.drawer,
      }}
    >
      <div className={classes.drawerHeader}>
        <IconButton
          onClick={() => {
            distpatch(closeDrawer());
          }}
        >
          <ChevronLeft />
        </IconButton>
      </div>
      <List>
        <DisplayToolsTray />
        <Divider />
        {/* <BuildToolsTray />
        <Divider />
        <FTPUploadToolsTray />
        <Divider />
        <ValidationToolsTray />
        <Divider />
        <MediaBuyTray />
        <Divider />
        <NgrokToolsTray /> */}
      </List>
    </Drawer>
  );
};

export default EmailToolsFrame;
