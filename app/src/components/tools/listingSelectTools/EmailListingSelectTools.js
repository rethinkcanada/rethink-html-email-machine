import React from "react";
import { Tooltip, IconButton } from "@material-ui/core";
import { CloudUpload, BusinessCenter, GridOn } from "@material-ui/icons";
import { useDispatch, useSelector } from "react-redux";

import { makeStyles } from "@material-ui/core/styles";
import {
  buildAllBanners,
  zipbuildAllBanners,
  ftpUploadAllBanners,
} from "../../store/actions/BatchProcessActions";

import { unslugify } from "../folderTools";

const useStyles = makeStyles((theme) => ({
  root: {
    float: "right",
  },
}));

const EmailListingSelectTools = ({ client, docket }) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const { selected } = useSelector((state) => ({
    ...state.bannerListReducer,
  }));

  return (
    <div className={classes.root}>
      <Tooltip title="Build">
        <IconButton
          onClick={() =>
            dispatch(
              buildAllBanners(
                `${unslugify(client)}/${unslugify(docket)}`,
                selected
              )
            )
          }
        >
          <GridOn />
        </IconButton>
      </Tooltip>
      <Tooltip title="Zipbuild">
        <IconButton
          onClick={() =>
            dispatch(
              zipbuildAllBanners(
                `${unslugify(client)}/${unslugify(docket)}`,
                selected
              )
            )
          }
        >
          <BusinessCenter />
        </IconButton>
      </Tooltip>
      <Tooltip title="Upload">
        <IconButton
          onClick={() =>
            dispatch(
              ftpUploadAllBanners(
                `${unslugify(client)}/${unslugify(docket)}`,
                selected
              )
            )
          }
        >
          <CloudUpload />
        </IconButton>
      </Tooltip>
    </div>
  );
};

export default EmailListingSelectTools;
