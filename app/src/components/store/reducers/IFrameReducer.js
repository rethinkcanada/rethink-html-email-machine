import {
  REFRESH_IFRAME,
  BROWSERSYNC_READY,
  SET_IFRAME_REF,
  START_DEV,
  RESET_EMAIL_DEV,
} from "../actions/actionTypes";

const initialState = {
  iframeRefCount: 0,
  emailfolder: "",
  emailURL: "",
  emailname: "",
  emailReady: false,
};

export default (state = initialState, action) => {
  // console.log
  switch (action.type) {
    case RESET_EMAIL_DEV:
      return { ...initialState };
    case START_DEV:
      return {
        ...state,
        emailfolder: action.payload.fullpath,
        emailname: action.payload.emailname,
      };
    case REFRESH_IFRAME:
      return {
        ...state,
        iframeRefCount: state.iframeRefCount + 1,
      };
    case BROWSERSYNC_READY:
      return {
        ...state,
        emailURL: action.payload.location,
        emailReady: true,
      };
    case SET_IFRAME_REF:
      return {
        ...state,
        iFrameRef: action.ref,
      };
    default:
      return state;
  }
};
