import {
  START_NGROK,
  READY_NGROK,
  STOP_NGROK,
  RESET_BANNER_DEV
} from "../actions/actionTypes";

const initialState = {
  ngrokProcessing: false,
  ngrokRunning: false,
  ngrokLocation: ""
};

export default (state = initialState, action) => {
  switch (action.type) {
    case RESET_BANNER_DEV:
      return { ...initialState };
    case START_NGROK:
      return { ...state, ngrokProcessing: true };
    case READY_NGROK:
      return {
        ...state,
        ngrokRunning: true,
        ngrokProcessing: false,
        ngrokLocation: action.payload.location
      };
    case STOP_NGROK:
      return {
        ...state,
        ngrokProcessing: false,
        ngrokRunning: false,
        ngrokLocation: ""
      };
    default:
      return state;
  }
};
