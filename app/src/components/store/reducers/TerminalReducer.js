import {
  TOGGLE_TERMINAL_AUTOSCROLLING,
  OPEN_TERMINAL,
  CLOSE_TERMINAL,
  TOGGLE_TERMINAL,
  TERMINAL_PROCESS_UPDATE,
  RESET_BANNER_DEV
} from "../actions/actionTypes";

const initalState = {
  terminalAutoScrolling: true,
  terminalTrayOpen: false,
  logs: [
    {
      method: "info",
      data: ["Initializing..."]
    }
  ]
};

export default (state = initalState, action) => {
  switch (action.type) {
    case RESET_BANNER_DEV:
      return { ...initalState };
    case TOGGLE_TERMINAL_AUTOSCROLLING:
      return {
        ...state,
        terminalAutoScrolling: !state.terminalAutoScrolling
      };
    case TERMINAL_PROCESS_UPDATE:
      return { ...state, logs: [...state.logs, action.payload] };
    case OPEN_TERMINAL:
      return { ...state, terminalTrayOpen: true };
    case CLOSE_TERMINAL:
      return { ...state, terminalTrayOpen: false };
    case TOGGLE_TERMINAL:
      return { ...state, terminalTrayOpen: !state.terminalTrayOpen };
    default:
      break;
  }
  return state;
};
