import {
  RETURN_CLIENT_LIST,
  RETURN_DOCKET_LIST,
  RETURN_EMAIL_LIST,
  RESET_EMAIL_DEV,
  ADD_SELECTED,
  SET_SELECTED,
  RESET_SELECTED,
  UPDATE_FILTER,
} from "../actions/actionTypes";

const initialState = {
  client: "",
  clients: [],
  docket: "",
  dockets: [],
  banner: "",
  banners: [],
  selected: [],
  filter: "",
  ftplink: "",
};

const filterSelected = (selected, filter) => {
  const regexp = new RegExp(filter, "i");
  return selected.filter((name) => regexp.test(name));
};

const updateSelected = (selected, name) => {
  const selectedIndex = selected.indexOf(name);

  let newSelected = [];

  if (selectedIndex === -1) {
    newSelected = newSelected.concat(selected, name);
  } else if (selectedIndex === 0) {
    newSelected = newSelected.concat(selected.slice(1));
  } else if (selectedIndex === selected.length - 1) {
    newSelected = newSelected.concat(selected.slice(0, -1));
  } else if (selectedIndex > 0) {
    newSelected = newSelected.concat(
      selected.slice(0, selectedIndex),
      selected.slice(selectedIndex + 1)
    );
  }
  return newSelected;
};

export default (state = initialState, action) => {
  switch (action.type) {
    case RESET_EMAIL_DEV:
      return { ...initialState };
    case RETURN_CLIENT_LIST:
    case RETURN_DOCKET_LIST:
      return { ...state, ...action.payload, ftplink: "" };
    case RETURN_EMAIL_LIST:
      return { ...state, ...action.payload };
    case RESET_SELECTED:
      return { ...state, selected: [] };
    case SET_SELECTED:
      return { ...state, selected: action.selected };
    case ADD_SELECTED:
      const selected = updateSelected(state.selected, action.name);
      return { ...state, selected };
    case UPDATE_FILTER:
      const newSelected = filterSelected(state.selected, action.filter);
      return { ...state, filter: action.filter, selected: newSelected };
    default:
      return state;
  }
};
