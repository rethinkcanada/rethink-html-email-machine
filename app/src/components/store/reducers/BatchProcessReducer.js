import {
  BUILDALL_EMAILS,
  BUILDALL_EMAILS_COMPLETE,
  BUILDALL_EMAILS_PROCESS_UPDATE,
  ZIPBUILDALL_EMAILS,
  ZIPBUILDALL_EMAILS_COMPLETE,
  ZIPBUILDALL_EMAILS_PROCESS_UPDATE,
  FTPUPLOADALL_EMAILS,
  FTPUPLOADALL_EMAILS_COMPLETE,
  FTPUPLOADALL_EMAILS_PROCESS_UPDATE,
  RESET_BATCH_PROCESS,
} from "../actions/actionTypes";

const createList = (list) => {
  return list.map((item) => {
    return { name: item, status: "processing" };
  });
};
const initialState = {
  batchProcessType: "",
  batchProcessDialogOpen: false,
  batchProcessing: false,
  batchComplete: false,
  batchProcessList: [],
};

export default (state = initialState, action) => {
  let newList = [];
  switch (action.type) {
    case BUILDALL_EMAILS:
      newList = createList(action.payload.emailsList);
      return {
        ...state,
        batchProcessType: "Build",
        batchProcessing: true,
        batchProcessList: newList,
        batchProcessDialogOpen: true,
      };
    case ZIPBUILDALL_EMAILS:
      newList = createList(action.payload.emailsList);
      return {
        ...state,
        batchProcessType: "Zip Build",
        batchProcessing: true,
        batchProcessList: newList,
        batchProcessDialogOpen: true,
      };
    case FTPUPLOADALL_EMAILS:
      newList = createList(action.payload.emailsList);
      return {
        ...state,
        batchProcessType: "FTP Upload",
        batchProcessing: true,
        batchProcessList: newList,
        batchProcessDialogOpen: true,
      };

    case BUILDALL_EMAILS_PROCESS_UPDATE:
    case ZIPBUILDALL_EMAILS_PROCESS_UPDATE:
    case FTPUPLOADALL_EMAILS_PROCESS_UPDATE:
      newList = state.batchProcessList.map((banner) => {
        if (banner.name === action.payload.bannerDone) {
          banner.status = action.payload.status;
          return banner;
        } else {
          return banner;
        }
      });
      return { ...state, batchProcessList: newList };

    case BUILDALL_EMAILS_COMPLETE:
    case ZIPBUILDALL_EMAILS_COMPLETE:
    case FTPUPLOADALL_EMAILS_COMPLETE:
      return { ...state, batchProcessing: false, batchComplete: true };

    case RESET_BATCH_PROCESS:
      return { ...initialState };
    default:
      return state;
  }
};
