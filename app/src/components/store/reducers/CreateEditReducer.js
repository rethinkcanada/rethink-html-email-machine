import {
  CREATE_EMAIL,
  CREATE_DOCKET_FOLDER,
  CREATE_CLIENT_FOLDER,
  CREATE_EDIT_COMPLETE,
  CREATE_EDIT_ERROR,
  CREATE_EDIT_RESET,
} from "../actions/actionTypes";

const initalState = {
  createEditProcessing: false,
  createEditSuccess: false,
  createEditError: false,
  createEditErrorMessage: "",
};

export default (state = initalState, action) => {
  switch (action.type) {
    case CREATE_EMAIL:
    case CREATE_DOCKET_FOLDER:
    case CREATE_CLIENT_FOLDER:
      return {
        ...state,
        createEditProcessing: true,
        createEditSuccess: false,
        createEditError: false,
        createEditErrorMessage: "",
      };
    case CREATE_EDIT_COMPLETE:
      return {
        ...state,
        createEditProcessing: false,
        createEditSuccess: true,
        createEditError: false,
        createEditErrorMessage: "",
      };
    case CREATE_EDIT_ERROR:
      return {
        ...state,
        createEditProcessing: false,
        createEditSuccess: false,
        createEditError: true,
        createEditErrorMessage: action.error,
      };
    case CREATE_EDIT_RESET:
      return initalState;
    default:
      return state;
  }
};
