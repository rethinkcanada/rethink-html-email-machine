import { combineReducers } from "redux";

import iFrameReducer from "./reducers/IFrameReducer";
import ngrokReducer from "./reducers/NgrokReducer";
import terminalReducer from "./reducers/TerminalReducer";
// import buildReducer from "./reducers/BuildReducer";
import emailListReducer from "./reducers/EmailListReducer";
import toolsReducer from "./reducers/ToolsReducer";
import createEditReducer from "./reducers/CreateEditReducer";
// import ftpUploadReducer from "./reducers/FTPUploadReducer";
import batchProcessReducer from "./reducers/BatchProcessReducer";

const rootReducer = combineReducers({
  iFrameReducer,
  terminalReducer,
  ngrokReducer,
  // buildReducer,
  emailListReducer,
  toolsReducer,
  createEditReducer,
  // ftpUploadReducer,
  batchProcessReducer,
});

export default rootReducer;
