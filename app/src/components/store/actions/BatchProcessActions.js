import {
  BUILDALL_BANNERS,
  ZIPBUILDALL_BANNERS,
  FTPUPLOADALL_BANNERS,
  RESET_BATCH_PROCESS
} from "./actionTypes";

// for batch building in the listing view
export const buildAllBanners = (folderDirectory, bannersList) => {
  return {
    type: BUILDALL_BANNERS,
    payload: { folderDirectory, bannersList }
  };
};

export const zipbuildAllBanners = (folderDirectory, bannersList) => {
  return {
    type: ZIPBUILDALL_BANNERS,
    payload: { folderDirectory, bannersList }
  };
};

export const ftpUploadAllBanners = (folderDirectory, bannersList) => {
  return {
    type: FTPUPLOADALL_BANNERS,
    payload: { folderDirectory, bannersList }
  };
};

export const resetBatchProcess = () => {
  return {
    type: RESET_BATCH_PROCESS
  };
};
