import {
  TERMINAL_PROCESS_UPDATE,
  REQUEST_BUILD,
  REQUEST_ZIPBUILD,
  REQUEST_STATIC_BACKUP,
  SAVE_STATIC_BACKUP,
  REBUILD_SPRITESHEET,
  EXTRACT_VIDEO_FRAMES,
  REBUILD_FLIPBOOK,
  SET_FRAME_EXTRACT_COUNT,
} from "./actionTypes";

export const requestStaticFeedback = () => {
  return {
    type: TERMINAL_PROCESS_UPDATE,
    payload: {
      method: "warn",
      data: ["Making Static Backup..."],
    },
  };
};

export const requestStatic = () => {
  return {
    type: REQUEST_STATIC_BACKUP,
  };
};

export const rebuildSpritesheetFeedback = () => {
  return {
    type: TERMINAL_PROCESS_UPDATE,
    payload: {
      method: "warn",
      data: ["Forcing rebuild of Spritesheet..."],
    },
  };
};

export const extractFramesFromVideosFeedback = (numframes) => {
  return {
    type: TERMINAL_PROCESS_UPDATE,
    payload: {
      method: "warn",
      data: ["Extracting " + numframes + " video frames"],
    },
  };
};

export const extractFramesFromVideo = (bannerfolder, numframes) => {
  return {
    type: EXTRACT_VIDEO_FRAMES,
    payload: {
      bannerfolder,
      numframes,
    },
  };
};

export const rebuildFlipbookFeedback = () => {
  return {
    type: TERMINAL_PROCESS_UPDATE,
    payload: {
      method: "warn",
      data: ["Forcing rebuilding of Flipbooks..."],
    },
  };
};

export const rebuildFlipbook = (bannerfolder) => {
  return {
    type: REBUILD_FLIPBOOK,
    payload: {
      bannerfolder,
    },
  };
};

export const rebuildSpritesheet = (bannerfolder) => {
  return {
    type: REBUILD_SPRITESHEET,
    payload: {
      bannerfolder,
    },
  };
};

export const saveStatic = (bannerfolder, bannername, data) => {
  return {
    type: SAVE_STATIC_BACKUP,
    payload: {
      bannerfolder,
      bannername,
      data,
    },
  };
};

export const buildBannerFeedback = () => {
  return {
    type: TERMINAL_PROCESS_UPDATE,
    payload: {
      method: "warn",
      data: ["Building banner..."],
    },
  };
};

export const buildBanner = (bannerfolder, bannername) => {
  return {
    type: REQUEST_BUILD,
    payload: { bannerfolder, bannername },
  };
};

export const zipbuildBannerFeedback = () => {
  return {
    type: TERMINAL_PROCESS_UPDATE,
    payload: {
      method: "warn",
      data: ["Zipping build..."],
    },
  };
};

export const zipbuildBanner = (bannerfolder, bannername) => {
  console.log("bannername: " + bannername);
  return {
    type: REQUEST_ZIPBUILD,
    payload: { bannerfolder, bannername },
  };
};
