import { TOGGLE_DRAWER, OPEN_DRAWER, CLOSE_DRAWER } from "./actionTypes";

export const toggleDrawer = () => {
  return { type: TOGGLE_DRAWER };
};

export const openDrawer = () => {
  return { type: OPEN_DRAWER };
};

export const closeDrawer = () => {
  return { type: CLOSE_DRAWER };
};
