import {
  CREATE_EMAIL,
  CREATE_EDIT_RESET,
  CREATE_DOCKET_FOLDER,
  CREATE_CLIENT_FOLDER,
} from "./actionTypes";

export const createClient = (data) => {
  return { type: CREATE_CLIENT_FOLDER, payload: { ...data } };
};

export const createDocket = (data) => {
  return { type: CREATE_DOCKET_FOLDER, payload: { ...data } };
};

export const createEmail = (data) => {
  return { type: CREATE_EMAIL, payload: { ...data } };
};

export const resetCreateEdit = () => {
  return { type: CREATE_EDIT_RESET };
};
