import {
  REFRESH_IFRAME,
  START_DEV,
  STOP_DEV,
  RESET_EMAIL_DEV,
} from "./actionTypes";

export const startDev = (fullpath, emailname) => {
  return {
    type: START_DEV,
    payload: { fullpath, emailname },
  };
};

export const stopDev = () => {
  return {
    type: STOP_DEV,
  };
};

export const refreshIFrame = () => {
  return {
    type: REFRESH_IFRAME,
  };
};

export const resetBannerDev = () => {
  return {
    type: RESET_EMAIL_DEV,
  };
};
