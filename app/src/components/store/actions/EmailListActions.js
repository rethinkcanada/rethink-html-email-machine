import {
  GET_CLIENT_LIST,
  GET_DOCKET_LIST,
  GET_EMAIL_LIST,
  SET_CLIENT,
  ADD_SELECTED,
  SET_SELECTED,
  RESET_SELECTED,
  UPDATE_FILTER,
} from "./actionTypes";

export const getClientList = () => {
  return {
    type: GET_CLIENT_LIST,
  };
};

export const getDocketList = (client) => {
  return {
    type: GET_DOCKET_LIST,
    client,
  };
};

export const getEmailList = (client, docket) => {
  return {
    type: GET_EMAIL_LIST,
    client,
    docket,
  };
};

export const setClient = (client) => {
  return {
    type: SET_CLIENT,
    client,
  };
};

export const addSelected = (name) => {
  return {
    type: ADD_SELECTED,
    name,
  };
};

export const setSelected = (selected) => {
  return {
    type: SET_SELECTED,
    selected,
  };
};

export const resetSelected = () => {
  return {
    type: RESET_SELECTED,
  };
};

export const updateFilter = (filter) => {
  return {
    type: UPDATE_FILTER,
    filter,
  };
};
