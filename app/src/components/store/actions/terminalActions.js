import {
  OPEN_TERMINAL,
  CLOSE_TERMINAL,
  TOGGLE_TERMINAL,
  TOGGLE_TERMINAL_AUTOSCROLLING
} from "./actionTypes";

export const toggleTerminal = () => {
  return { type: TOGGLE_TERMINAL };
};

export const openTerminal = () => {
  return { type: OPEN_TERMINAL };
};

export const closeTerminal = () => {
  return { type: CLOSE_TERMINAL };
};

export const toggleTerminalAutoscrolling = () => {
  return { type: TOGGLE_TERMINAL_AUTOSCROLLING };
};
