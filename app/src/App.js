import React from "react";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import { createStore, applyMiddleware, compose } from "redux";
import { Provider } from "react-redux";
import creatSocketIoMiddleware from "redux-socket.io";
import rootReducer from "./components/store/rootReducer";

import EmailListing from "./components/routes/EmailListing";
import EmailDev from "./components/routes/EmailDev";

import "typeface-roboto";

import { socket } from "./components/store/socket";

const middlewares = [
  // ReduxThunk,
  creatSocketIoMiddleware(socket, ["post/", "get/"]),
];
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(...middlewares))
);

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <BrowserRouter>
          <Switch>
            <Route path="/list/:client?/:docket?" component={EmailListing} />
            <Route
              exact
              path="/dev/:client/:docket/:emailfolder"
              component={EmailDev}
            />
            <Redirect from="/" to="/list" />
          </Switch>
        </BrowserRouter>
      </div>
    </Provider>
  );
}

export default App;
