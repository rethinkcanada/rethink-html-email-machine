const proxy = require("http-proxy-middleware");

module.exports = function (app) {
  app.use(proxy("/server/", { target: "http://localhost:6000/" }));
  app.use(proxy("/socket.io", { target: "ws://localhost:6000/", ws: true }));
  // app.use(proxy("/packages/**", { target: "http://localhost:6000/" }));
  // app.use(proxy("/8080", { target: "http://localhost:8080/" }));
  // app.use(proxy("/8081", { target: "http://localhost:8081/" }));
  // app.use(proxy("/8082", { target: "http://localhost:8082/" }));
  // app.use(proxy("/8083", { target: "http://localhost:8083/" }));
  // app.use(proxy("/8084", { target: "http://localhost:8084/" }));
  // app.use(proxy("/8085", { target: "http://localhost:8085/" }));
  // app.use(proxy("/8086", { target: "http://localhost:8086/" }));
  // app.use(proxy("/8087", { target: "http://localhost:8087/" }));
  // app.use(proxy("/8088", { target: "http://localhost:8088/" }));
  // app.use(proxy("/8089", { target: "http://localhost:8089/" }));
  // app.use(proxy("/8090", { target: "http://localhost:8090/" }));
};
