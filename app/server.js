const express = require("express");
const app = express();
const server = require("http").Server(app);
const io = require("socket.io")(server);
const bodyParser = require("body-parser");
const port = process.env.PORT || 6000;
const ENV = process.env.NODE_ENV || "development";

const { lstatSync, readdirSync, access, F_OK } = require("fs");
const { join, resolve } = require("path");

const { unslugify } = require("./src/components/tools/folderTools");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

if (ENV === "production") {
  app.use(express.static("build"));
}

io.on("connection", (socketClient) => {
  console.log("socket connected");
  const sharedVariables = {
    bannerfolder: "",
    port: null,
  };

  require("./server-components/sockets/emailListingSocket")(
    socketClient,
    sharedVariables
  );

  require("./server-components/sockets/createEditSocket")(
    socketClient,
    sharedVariables
  );

  // require("./server-components/sockets/startdevSocket")(
  //   socketClient,
  //   sharedVariables
  // );

  // require("./server-components/sockets/devingbuildSocket")(
  //   socketClient,
  //   sharedVariables
  // );
});

server.listen(port, () => {
  // runGulpTask("ping");
  console.log(
    `Rethink Banner development server running at http://localhost:${port}`
  );
});
