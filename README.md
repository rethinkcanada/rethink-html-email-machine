## Installation

To build the docker container, run `docker-compose up`

If you need to rebuild the container due to changes in the docker file, use the following command: `docker-compose up --build`, but if errors occur, try use `docker-compose up --no-cache --build` (rebuilds the image from the very beginning)

## Running Container

To start the script, first run `docker-compose up`
Then ssh into it by running `docker exec -it rethink-html-email-generator /bin/zsh`
then run the command `npm run dev`

for banner listing visit http://localhost:3000

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Issues with time drift in the container

There is a common problem with the time drifting on docker containers running on mac systems, that go into, and wake from sleep mode.
The solution is to run another docker container that will update and sync the VM clock when the OS wakes up from sleep mode. `docker run --rm --privileged alpine hwclock -s` [Source article](https://blog.shameerc.com/2017/03/quick-tip-fixing-time-drift-issue-on-docker-for-mac). You need to run this command every time there is a drift, so there is an agent script that will do it for you here -> `curl https://raw.githubusercontent.com/arunvelsriram/docker-time-sync-agent/master/install.sh | bash`

## Automated ftp uploading

To have this part of the automation process, a json file needs to be present in the folder and name `app/keys/ftp-cred.json`. NEVER COMMIT THIS FILE TO THE GIT REPO!
It's structure looks like the following:

```
{
  "host": "{hostname}",
  "user": "{username}",
  "password": "{password}"
}
```

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
